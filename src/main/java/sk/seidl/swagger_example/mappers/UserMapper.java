package sk.seidl.swagger_example.mappers;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import sk.seidl.swagger_example.model.User;
import sk.seidl.swagger_example.model.dto.UserDto;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-19
 */
@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mappings({
            @Mapping(source = "firstName", target = "name"),
            @Mapping(source = "lastName", target = "surname")
    })
    UserDto userToDto(User user);

    @InheritInverseConfiguration
    User dtoToUser(UserDto userDto);
}
