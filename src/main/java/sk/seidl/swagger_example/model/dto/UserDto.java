package sk.seidl.swagger_example.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-19
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    @ApiModelProperty(value = "This is first Name", required = true)
    private String name;

    private String surname;
}
