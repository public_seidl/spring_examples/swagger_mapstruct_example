package sk.seidl.swagger_example.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-19
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {// extends WebMvcConfigurationSupport {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .apiInfo(metaData());
    }

    private ApiInfo metaData() {
        Contact contact = new Contact("Matus Seidl", "upteam.sk", "seidl.matus@gmail.com");
        return new ApiInfo("Swagger Example",
                "Swagger Example  with MapStruc and Swagger UI",
                "1.0",
                "terms of service",
                contact,
                "Licencia",
                "url.licencia.com", Collections.emptyList());
    }


}
