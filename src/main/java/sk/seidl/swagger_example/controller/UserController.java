package sk.seidl.swagger_example.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import sk.seidl.swagger_example.mappers.UserMapper;
import sk.seidl.swagger_example.model.User;
import sk.seidl.swagger_example.model.dto.UserDto;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-19
 */
@Api(description = "This is my User Controller")
@RestController
public class UserController {

    UserMapper userMapper = UserMapper.INSTANCE;


    @GetMapping("/user")
    @ResponseStatus(HttpStatus.OK)
    public UserDto getUser() {
        User user = new User("Janko", "Hrasko");
        return userMapper.userToDto(user);
    }

    @ApiOperation(value = "this will get a User", notes = "This is notes for User")
    @PostMapping("/user")
    @ResponseStatus(HttpStatus.OK)
    public UserDto createUser(User user) {
        return userMapper.userToDto(user);
    }

    @DeleteMapping("/user")
    @ResponseStatus(HttpStatus.OK)
    public UserDto deleteUser(User user) {
        return userMapper.userToDto(user);
    }

    @PutMapping("/user")
    @ResponseStatus(HttpStatus.OK)
    public UserDto updateUser(User user) {
        user.setFirstName("Igor");
        return userMapper.userToDto(user);
    }

}
