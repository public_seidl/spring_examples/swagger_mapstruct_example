package sk.seidl.swagger_example.mappers;

import org.junit.Test;
import sk.seidl.swagger_example.model.User;
import sk.seidl.swagger_example.model.dto.UserDto;

import static org.junit.Assert.*;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-19
 */
public class UserMapperTest {

    @Test
    public void userToDto() {
        User user = new User("Janko","Hrasko");
        UserDto dto = UserMapper.INSTANCE.userToDto(user);

        assertEquals(user.getFirstName(),dto.getName());
        assertEquals(user.getLastName(),dto.getSurname());
    }

    @Test
    public void testdtoToUser() {
        UserDto dto = new UserDto("Janko","Hrasko");
        User user = UserMapper.INSTANCE.dtoToUser(dto);

        assertEquals(dto.getName(),user.getFirstName());
        assertEquals(dto.getSurname(),user.getLastName());
    }
}